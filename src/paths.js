export const paths = {
  index: "/auth/jwt/login",
  auth: {
    jwt: {
      login: "/auth/jwt/login",
      // register: "/auth/jwt/register",
    },
  },
  dashboard: {
    index: "/dashboard",
    account: "/dashboard/account",
    blank: "/dashboard/blank",
    blog: {
      index: "/dashboard/blog",
      postDetails: "/dashboard/blog/:postId",
      postCreate: "/dashboard/blog/create",
    },
    customers: {
      index: "/dashboard/customers",
      details: "/dashboard/customers/:customerId",
      edit: "/dashboard/customers/:customerId/edit",
    },
  },
  401: "/401",
  404: "/404",
  500: "/500",
};
