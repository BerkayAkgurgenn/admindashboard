const INITIAL_STATE = {
  user: {
    username: "",
    isAdmin: false,
  },
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "CHANGE_USER":
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

export default userReducer;
