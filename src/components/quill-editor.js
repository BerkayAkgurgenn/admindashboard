import "react-quill/dist/quill.snow.css";
import { useRef } from "react";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";

const Quill = dynamic(() => import("react-quill"), {
  ssr: false,
  loading: () => null,
});

const QuillEditorRoot = styled("div")(({ theme }) => ({
  border: 1,
  borderColor: theme.palette.divider,
  borderRadius: theme.shape.borderRadius,
  borderStyle: "solid",
  display: "flex",
  flexDirection: "column",
  overflow: "hidden",
  "& .quill": {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    overflow: "hidden",
  },
  "& .ql-snow.ql-toolbar": {
    borderColor: theme.palette.divider,
    borderLeft: "none",
    borderRight: "none",
    borderTop: "none",
    "& .ql-picker-label:hover": {
      color: theme.palette.primary.main,
    },
    "& .ql-picker-label.ql-active": {
      color: theme.palette.primary.main,
    },
    "& .ql-picker-item:hover": {
      color: theme.palette.primary.main,
    },
    "& .ql-picker-item.ql-selected": {
      color: theme.palette.primary.main,
    },
    "& button:hover": {
      color: theme.palette.primary.main,
      "& .ql-stroke": {
        stroke: theme.palette.primary.main,
      },
    },
    "& button:focus": {
      color: theme.palette.primary.main,
      "& .ql-stroke": {
        stroke: theme.palette.primary.main,
      },
    },
    "& button.ql-active": {
      "& .ql-stroke": {
        stroke: theme.palette.primary.main,
      },
    },
    "& .ql-stroke": {
      stroke: theme.palette.text.primary,
    },
    "& .ql-picker": {
      color: theme.palette.text.primary,
    },
    "& .ql-picker-options": {
      backgroundColor: theme.palette.background.paper,
      border: "none",
      borderRadius: theme.shape.borderRadius,
      boxShadow: theme.shadows[10],
      padding: theme.spacing(2),
    },
  },
  "& .ql-snow.ql-container": {
    borderBottom: "none",
    borderColor: theme.palette.divider,
    borderLeft: "none",
    borderRight: "none",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    height: "auto",
    overflow: "hidden",
    "& .ql-editor": {
      color: theme.palette.text.primary,
      flex: 1,
      fontFamily: theme.typography.body1.fontFamily,
      fontSize: theme.typography.body1.fontSize,
      height: "auto",
      overflowY: "auto",
      padding: theme.spacing(2),
      "&.ql-blank::before": {
        color: theme.palette.text.secondary,
        fontStyle: "normal",
        left: theme.spacing(2),
      },
    },
  },
}));

const modules = {
  toolbar: [
    [{ header: [1, 2, false] }, { font: [] }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image", "video"],
    ["clean"],
  ],
};

const formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "align",
  "strike",
  "script",
  "blockquote",
  "background",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
  "color",
  "code-block",
];

const imageHandler = () => {
  const input = document.createElement("input");

  input.setAttribute("type", "file");
  input.setAttribute("accept", "image/*");
  input.click();

  input.onchange = async () => {
    var file = input.files[0];
    var formData = new FormData();

    formData.append("image", file);

    var fileName = file.name;

    const res = await uploadFiles(file, fileName, quillObj);
  };
};

const uploadFiles = (uploadFileObj, filename, quillObj) => {
  var libraryName = "ImageFiles";
  var context = props.context;
  var siteUrl = props.context.pageContext.site.absoluteUrl;

  var currentdate = new Date();
  var fileNamePredecessor =
    currentdate.getDate().toString() +
    currentdate.getMonth().toString() +
    currentdate.getFullYear().toString() +
    currentdate.getTime().toString();

  filename = fileNamePredecessor + filename;

  //To Upload in root folder
  var apiUrl = `${siteUrl}/_api/Web/Lists/getByTitle('${libraryName}')/RootFolder/Files/Add(url='${filename}', overwrite=true)`;
  const digestCache = props.context.serviceScope.consume(
    DigestCache.serviceKey
  );
  digestCache
    .fetchDigest(props.context.pageContext.web.serverRelativeUrl)
    .then(async (digest) => {
      try {
        if (uploadFileObj != "") {
          fetch(apiUrl, {
            method: "POST",
            headers: {
              "Content-Type": "application/json;odata=verbose",
              "X-RequestDigest": digest,
            },
            body: uploadFileObj, // This is your file object
          })
            .then((response) => {
              console.log(response);
              const range = quillObj.getEditorSelection();

              var res = siteUrl + "/" + listName + "/" + filename;

              quillObj.getEditor().insertEmbed(range.index, "image", res);
            })
            .catch((error) => console.log(error));
        }
      } catch (error) {
        console.log("uploadFiles : " + error);
      }
    });
};

export const QuillEditor = (props) => {
  const { sx, onChange, placeholder, value, ...other } = props;
  const ref = useRef(null);

  return (
    <QuillEditorRoot sx={sx} ref={ref} {...other}>
      <Quill
        onChange={onChange}
        placeholder={placeholder}
        value={value}
        formats={formats}
        modules={modules}
        handlers={{
          image: imageHandler,
        }}
        bounds={ref.current || undefined}
      />
    </QuillEditorRoot>
  );
};

QuillEditor.propTypes = {
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  sx: PropTypes.object,
  value: PropTypes.string,
  fotmats: PropTypes.object,
};
