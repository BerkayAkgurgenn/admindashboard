import { deepCopy } from "../../utils/deep-copy";
import { post, posts } from "./data";

class BlogApi {
  getPosts(request) {
    let blogList = fetch("https://deryamsaglik.arifozbey.com/DrBircan/BlogGet")
      .then((res) => res.json())
      .then((data) => deepCopy(data));
    return blogList;
  }

  getPost(blogId) {
    let blogList = fetch(
      `https://deryamsaglik.arifozbey.com/DrBircan/BlogGetbyId?id=${blogId}`
    )
      .then((res) => res.json())
      .then((data) => deepCopy(data));
    return blogList;
  }
}

export const blogApi = new BlogApi();
