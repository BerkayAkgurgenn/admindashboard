import { useCallback, useEffect, useState } from "react";
import Head from "next/head";
import NextLink from "next/link";
import DotsHorizontalIcon from "@untitled-ui/icons-react/build/esm/DotsHorizontal";
import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  CardContent,
  Container,
  IconButton,
  Link,
  Stack,
  SvgIcon,
  TextField,
  Typography,
  Unstable_Grid2 as Grid,
} from "@mui/material";
import { BreadcrumbsSeparator } from "../../../components/breadcrumbs-separator";
import { QuillEditor } from "../../../components/quill-editor";
import { usePageView } from "../../../hooks/use-page-view";
import { Layout as DashboardLayout } from "../../../layouts/dashboard";
import { paths } from "../../../paths";
import { useRouter } from "next/navigation";

const Page = () => {
  const [cover, setCover] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [seoTitle, setSeoTitle] = useState("");
  const [seoDesc, setSeoDesc] = useState("");
  const [shortDesc, setShortDesc] = useState("");
  const [editMode, setEditMode] = useState(false);
  const router = useRouter();

  usePageView();

  // TODO 1 THIS SECTION WILL BE ADD WHEN SERVICES CHANGED
  // const handleCoverDrop = useCallback(async ([file]) => {
  //   const data = await fileToBase64(file);
  //   setCover(data);
  // }, []);

  // const handleCoverRemove = useCallback(() => {
  //   setCover(null);
  // }, []);

  const sendNewBlog = () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const blogId = urlParams.get("id");
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    if (!editMode) {
      let raw = JSON.stringify({
        header: title,
        name: title,
        url: cover,
        shortDesc: shortDesc,
        seotitle: seoTitle,
        seodescriptiom: seoDesc,
        actionName: content,
        date: new Date().toISOString().split("T")[0],
      });
      let requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      if (title.length !== 0 || content.length !== 0) {
        fetch(
          `https://deryamsaglik.arifozbey.com/DrBircan/BlogCreate`,
          requestOptions
        )
          .then((res) => res.json())
          .then((data) =>
            router.push(`/dashboard/blog/${data.id}?id=${data.id}`)
          );
      }
    } else {
      let raw = JSON.stringify({
        id: blogId,
        header: title,
        name: title,
        url: cover,
        shortDesc: shortDesc,
        seotitle: seoTitle,
        seodescriptiom: seoDesc,
        actionName: content,
        date: new Date().toISOString().split("T")[0],
      });
      let requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      if (title.length !== 0 || content.length !== 0) {
        fetch(
          `https://deryamsaglik.arifozbey.com/DrBircan/BlogEdit`,
          requestOptions
        )
          .then((res) => res.json())
          .then((data) => {
            router.push(`/dashboard/blog/${data.id}?id=${data.id}`);
          });
      }
    }
  };

  useEffect(() => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const blogId = urlParams.get("id");
    if (blogId !== null) {
      fetch(
        `https://deryamsaglik.arifozbey.com/DrBircan/BlogGetbyId?id=${blogId}`
      )
        .then((resp) => resp.json())
        .then((data) => {
          setCover(data.url);
          setTitle(data.header);
          setContent(data.actionName);
          setSeoTitle(data.seotitle);
          setSeoDesc(data.seodescription);
          setShortDesc(data.shortdesc);
          setEditMode(true);
        });
    }
  }, []);

  return (
    <>
      <Head>
        <title>Blog: Post Create | Center of Surgeons</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Stack spacing={1}>
            <Typography variant="h3">Create a new post</Typography>
            <Breadcrumbs separator={<BreadcrumbsSeparator />}>
              <Link
                color="text.primary"
                component={NextLink}
                href={paths.dashboard.index}
                variant="subtitle2"
              >
                Dashboard
              </Link>
              <Link
                color="text.primary"
                component={NextLink}
                href={paths.dashboard.blog.index}
                variant="subtitle2"
              >
                Blog
              </Link>
              <Typography color="text.secondary" variant="subtitle2">
                Create
              </Typography>
            </Breadcrumbs>
          </Stack>
          <Card
            elevation={16}
            sx={{
              alignItems: "center",
              borderRadius: 1,
              display: "flex",
              justifyContent: "space-between",
              mb: 8,
              mt: 6,
              px: 3,
              py: 2,
            }}
          >
            <Typography variant="subtitle1">Hello, Admin</Typography>
            <Stack alignItems="center" direction="row" spacing={2}>
              <Button
                color="inherit"
                component={NextLink}
                href={paths.dashboard.blog.index}
              >
                Cancel
              </Button>
              <Button
                // component={NextLink}
                // href={paths.dashboard.blog.postDetails}
                onClick={sendNewBlog}
                variant="contained"
              >
                Publish changes
              </Button>
              <IconButton>
                <SvgIcon>
                  <DotsHorizontalIcon />
                </SvgIcon>
              </IconButton>
            </Stack>
          </Card>
          <Stack spacing={3}>
            <Card>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid xs={12} md={4}>
                    <Typography variant="h6">Basic details</Typography>
                  </Grid>
                  <Grid xs={12} md={8}>
                    <Stack spacing={3}>
                      <TextField
                        fullWidth
                        label="Post title"
                        name="title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                      <TextField
                        fullWidth
                        label="Short description"
                        value={shortDesc}
                        onChange={(e) => setShortDesc(e.target.value)}
                      />
                    </Stack>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
            <Card>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid xs={12} md={4}>
                    <Typography variant="h6">Post cover</Typography>
                  </Grid>
                  <Grid xs={12} md={8}>
                    <Stack spacing={3}>
                      {/* {cover ? (
                        <Box
                          sx={{
                            backgroundImage: `url(${cover})`,
                            backgroundPosition: "center",
                            backgroundSize: "cover",
                            borderRadius: 1,
                            height: 230,
                            mt: 3,
                          }}
                        />
                      ) : (
                        <Box
                          sx={{
                            alignItems: "center",
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            border: 1,
                            borderRadius: 1,
                            borderStyle: "dashed",
                            borderColor: "divider",
                            height: 230,
                            mt: 3,
                            p: 3,
                          }}
                        >
                          <Typography
                            align="center"
                            color="text.secondary"
                            variant="h6"
                          >
                            Select a cover image
                          </Typography>
                          <Typography
                            align="center"
                            color="text.secondary"
                            sx={{ mt: 1 }}
                            variant="subtitle1"
                          >
                            Image used for the blog post cover and also for Open
                            Graph meta
                          </Typography>
                        </Box>
                      )}
                      <div>
                        <Button
                          color="inherit"
                          disabled={!cover}
                          onClick={handleCoverRemove}
                        >
                          Remove photo
                        </Button>
                      </div>
                      <FileDropzone
                        accept={{ "image/*": [] }}
                        maxFiles={1}
                        onDrop={handleCoverDrop}
                        caption="(SVG, JPG, PNG, or gif maximum 900x400)"
                      /> */}
                      <TextField
                        fullWidth
                        label="Post Cover"
                        name="cover"
                        value={cover}
                        onChange={(e) => setCover(e.target.value)}
                      />
                    </Stack>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
            <Card>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid xs={12} md={4}>
                    <Typography variant="h6">Content</Typography>
                  </Grid>
                  <Grid xs={12} md={8}>
                    <QuillEditor
                      placeholder="Write something"
                      value={content}
                      onChange={setContent}
                      sx={{ height: 330 }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
            <Card>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid xs={12} md={4}>
                    <Typography variant="h6">Meta</Typography>
                  </Grid>
                  <Grid xs={12} lg={8}>
                    <Stack spacing={3}>
                      <TextField
                        fullWidth
                        value={seoTitle}
                        onChange={(e) => setSeoTitle(e.target.value)}
                        label="SEO title"
                        name="title"
                      />
                      <TextField
                        fullWidth
                        label="SEO description"
                        value={seoDesc}
                        onChange={(e) => setSeoDesc(e.target.value)}
                      />
                    </Stack>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Stack>
          <Box
            sx={{
              display: {
                sm: "none",
              },
              mt: 2,
            }}
          >
            <Button onClick={sendNewBlog} variant="contained">
              Publish changes
            </Button>
          </Box>
        </Container>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
