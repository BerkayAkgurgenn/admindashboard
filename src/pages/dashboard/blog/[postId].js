import { useCallback, useEffect, useState } from "react";
import Head from "next/head";
import NextLink from "next/link";
import { format, subHours } from "date-fns";
import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Chip,
  Container,
  Divider,
  Link,
  Stack,
  Typography,
} from "@mui/material";
import { blogApi } from "../../../api/blog";
import { BreadcrumbsSeparator } from "../../../components/breadcrumbs-separator";
import { useMounted } from "../../../hooks/use-mounted";
import { usePageView } from "../../../hooks/use-page-view";
import { Layout as DashboardLayout } from "../../../layouts/dashboard";
import { paths } from "../../../paths";
import { PostContent } from "../../../sections/dashboard/blog/post-content";
import { useRouter } from "next/router";

const usePost = () => {
  const isMounted = useMounted();
  const [post, setPost] = useState(null);

  const getPost = useCallback(async () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const blogId = urlParams.get("id");
    try {
      const response = await blogApi.getPost(blogId);

      if (isMounted()) {
        setPost(response);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMounted]);

  useEffect(
    () => {
      getPost();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return post;
};

const Page = () => {
  const post = usePost();
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const blogId = urlParams.get("id");
  const router = useRouter();

  usePageView();

  if (!post) {
    return null;
  }

  const deletePost = () => {
    fetch(
      `https://deryamsaglik.arifozbey.com/DrBircan/BlogDelete?Id=${blogId}`,
      {
        method: "POST",
      }
    )
      .then((res) => res.json())
      .then(() => router.push(`/dashboard/blog`));
  };

  return (
    <>
      <Head>
        <title>Blog: Post Details | Center of Surgeons</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Stack spacing={1}>
            <Typography variant="h3">Post</Typography>
            <Breadcrumbs separator={<BreadcrumbsSeparator />}>
              <Link
                color="text.primary"
                component={NextLink}
                href={paths.dashboard.index}
                variant="subtitle2"
              >
                Dashboard
              </Link>
              <Link
                color="text.primary"
                component={NextLink}
                href={paths.dashboard.blog.index}
                variant="subtitle2"
              >
                Blog
              </Link>
              <Typography color="text.secondary" variant="subtitle2">
                Details
              </Typography>
            </Breadcrumbs>
          </Stack>
          <Card
            elevation={16}
            sx={{
              alignItems: "center",
              borderRadius: 1,
              display: "flex",
              justifyContent: "space-between",
              mb: 8,
              mt: 6,
              px: 3,
              py: 2,
            }}
          >
            <Typography variant="subtitle1">Hello, Admin</Typography>
            <div>
              <Button
                onClick={deletePost}
                variant="contained"
                style={{ marginRight: "30px" }}
              >
                Delete Post
              </Button>
              <Button
                component={NextLink}
                href={`/dashboard/blog/create?id=${blogId}`}
                variant="contained"
              >
                Edit Post
              </Button>
            </div>
          </Card>
          <Stack spacing={3}>
            {/* <div>
              <Chip label={post.category} />
            </div> */}
            <Typography variant="h3">{post.header}</Typography>
            {/* <Typography color="text.secondary" variant="subtitle1">
              {post.shortDescription}
            </Typography> */}
            {/* <Stack
              alignItems="center"
              direction="row"
              spacing={2}
              sx={{ mt: 3 }}
            >
              <Avatar src={post.author.avatar} />
              <div>
                <Typography variant="subtitle2">
                  By {post.author.name} • {publishedAt}
                </Typography>
                <Typography color="text.secondary" variant="body2">
                  {post.readTime} read
                </Typography>
              </div>
            </Stack> */}
          </Stack>
          <Box
            sx={{
              backgroundImage: `url(${post.url})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              borderRadius: 1,
              height: 380,
              mt: 3,
            }}
          />
          {post.name && (
            <Container sx={{ py: 3 }}>
              <PostContent content={post.desc ? post.desc : post.actionName} />
            </Container>
          )}
          {/* <Divider sx={{ my: 3 }} /> */}
          {/* <Stack spacing={2}>
            {comments.map((comment) => (
              <PostComment
                key={comment.id}
                {...comment} />
            ))}
          </Stack>
          <Divider sx={{ my: 3 }} />
          <PostCommentAdd />
          <Box sx={{ mt: 8 }}>
            <PostNewsletter />
          </Box> */}
        </Container>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
