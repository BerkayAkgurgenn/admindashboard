import Head from "next/head";
import { usePageView } from "../hooks/use-page-view";
import { Layout as AuthLayout } from '../layouts/auth/modern-layout';
import Auth from "./auth/amplify/login";

const Page = () => {
  usePageView();

  return (
    <>
      <Head>
        <title>COS - Admin</title>
      </Head>
      <main>
        <Auth />
      </main>
    </>
  );
};

Page.getLayout = (page) => <AuthLayout>{page}</AuthLayout>;

export default Page;
