import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { useSettings } from "../../hooks/use-settings";
import { HorizontalLayout } from "./horizontal-layout";
import { VerticalLayout } from "./vertical-layout";
// import { getSections } from "./config";
import { withAuthGuard } from "../../hocs/with-auth-guard";
import { tokens } from "../../locales/tokens";
import { paths } from "../../paths";
import { SvgIcon } from "@mui/material";
import HomeSmileIcon from "../../icons/untitled-ui/duocolor/home-smile";
import LayoutAlt02Icon from "../../icons/untitled-ui/duocolor/layout-alt-02";
import Users03Icon from "../../icons/untitled-ui/duocolor/users-03";
import { useSelector } from "react-redux";

export const Layout = withAuthGuard((props) => {
  const settings = useSettings();
  const [sections, setSections] = useState([]);
  const { tenant } = useSelector((state) => state.tenant);

  useEffect(() => {
    if (localStorage.getItem("activeProject") === null) {
      localStorage.setItem("activeProject", "Doctor Bircan");
    }
  }, []);

  useEffect(() => {
    if (typeof window !== "undefined") {
      if (localStorage.getItem("activeProject") === "Doctor Bircan") {
        setSections([
          {
            items: [
              {
                title: tokens.nav.overview,
                path: paths.dashboard.index,
                icon: (
                  <SvgIcon fontSize="small">
                    <HomeSmileIcon />
                  </SvgIcon>
                ),
              },
            ],
          },
          {
            subheader: tokens.nav.pages,
            items: [
              {
                title: tokens.nav.blog,
                path: paths.dashboard.blog.index,
                icon: (
                  <SvgIcon fontSize="small">
                    <LayoutAlt02Icon />
                  </SvgIcon>
                ),
                items: [
                  {
                    title: tokens.nav.postList,
                    path: paths.dashboard.blog.index,
                  },

                  {
                    title: tokens.nav.postCreate,
                    path: paths.dashboard.blog.postCreate,
                  },
                ],
              },
            ],
          },
        ]);
      } else if (localStorage.getItem("activeProject") === "COSyLife App") {
        setSections([
          {
            items: [
              {
                title: tokens.nav.overview,
                path: paths.dashboard.index,
                icon: (
                  <SvgIcon fontSize="small">
                    <HomeSmileIcon />
                  </SvgIcon>
                ),
              },
            ],
          },
          {
            subheader: tokens.nav.pages,
            items: [
              {
                title: tokens.nav.customers,
                path: paths.dashboard.customers.index,
                icon: (
                  <SvgIcon fontSize="small">
                    <Users03Icon />
                  </SvgIcon>
                ),
                items: [
                  {
                    title: tokens.nav.list,
                    path: paths.dashboard.customers.index,
                  },
                ],
              },
              {
                title: tokens.nav.blog,
                path: paths.dashboard.blog.index,
                icon: (
                  <SvgIcon fontSize="small">
                    <LayoutAlt02Icon />
                  </SvgIcon>
                ),
                items: [
                  {
                    title: tokens.nav.postList,
                    path: paths.dashboard.blog.index,
                  },

                  {
                    title: tokens.nav.postCreate,
                    path: paths.dashboard.blog.postCreate,
                  },
                ],
              },
            ],
          },
        ]);
      }
    }
  }, [tenant]);

  if (settings.layout === "horizontal") {
    return (
      <HorizontalLayout
        sections={sections}
        navColor={settings.navColor}
        {...props}
      />
    );
  }

  return (
    <VerticalLayout
      sections={sections}
      navColor={settings.navColor}
      {...props}
    />
  );
});

Layout.propTypes = {
  children: PropTypes.node,
};
